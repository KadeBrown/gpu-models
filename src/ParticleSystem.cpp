#include "ParticleSystem.h"
#include "gl_core_4_4.h"
#include <iostream>
#include <string>
#include <fstream>
#include "FBXLoader\vs2015\FBXFile.h"
#include <algorithm>
#include <chrono>



ParticleSystem::ParticleSystem()
	: m_MaxParticles(0),
	m_Position(0, 0, 0), m_DrawShader(0),
	m_UpdateShader(0), m_LastDrawTime(0)
{
	m_VAO[0] = 0;
	m_VAO[1] = 0;
	m_VBO[0] = 0;
	m_VBO[1] = 0;
}


ParticleSystem::~ParticleSystem()
{

	delete[] m_Positions;

	delete[] m_Velocities;

	delete[] m_Targets;


	glDeleteVertexArrays(2, m_VAO);
	glDeleteBuffers(10, m_VBO);

	//Delete shaders
	glDeleteProgram(m_DrawShader);
	glDeleteProgram(m_UpdateShader);
}

void ParticleSystem::ConvertMeshToIterator(FBXFile* a_mesh)
{
	if (m_Positions != nullptr)
		delete[] m_Positions;

	if (m_Velocities != nullptr)
		delete[] m_Velocities;

	if (m_Targets != nullptr)
		delete[] m_Targets;



	m_MaxParticles = 0;
	
	for (FBXMeshNode* m : a_mesh->GetMeshes())
	{
		m_MaxParticles += m->m_vertices.size();
	}
	
	m_Positions = new vec3[m_MaxParticles];
	m_Velocities = new vec3[m_MaxParticles];
	m_Targets = new vec3[m_MaxParticles];

	memset(m_Positions, 0, m_MaxParticles * sizeof(vec3));
	memset(m_Velocities, 0, m_MaxParticles * sizeof(vec3));

	int i = 0;

	for (FBXMeshNode* m : a_mesh->GetMeshes())
	{
		//Create particle array

		for (int o = 0; o < m->m_vertices.size(); o++)
		{
			m_Targets[i] = vec3(m->m_vertices[o].position) * m_ModelScale;
			i++;
		}

	}
}

void ParticleSystem::NewModel(FBXFile* newModel)
{
	unsigned int oldMaxParticles = m_MaxParticles;
	ConvertMeshToIterator(newModel);

	unsigned int smallestMaxParticles = std::min(oldMaxParticles, m_MaxParticles);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[m_ActiveBuffer * m_BuffersPerObj + POSITION]);
	vec3* oldPositions = (vec3*)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);

	for (unsigned int i = 0; i < smallestMaxParticles; i++)
	{
		m_Positions[i] = oldPositions[i];
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);

	//glBufferSubData(GL_ARRAY_BUFFER, 0, smallestMaxParticles * sizeof(vec3), )

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	deltaTime = 0;
}

void ParticleSystem::SwitchModel(FBXFile * newModel)
{
	using namespace std::chrono;
	steady_clock::time_point startTime;
	steady_clock::time_point endTime;
	startTime = steady_clock::now();

	NewModel(newModel);

	FreeBuffers();
	CreateShaderBuffers();

	endTime = steady_clock::now();


	auto total = endTime - startTime;
	auto totalms = duration_cast<duration<float, std::milli>>(total);
	std::cout << totalms.count() << std::endl;
}

void ParticleSystem::FreeBuffers()
{
	glDeleteVertexArrays(2, m_VAO);
	glDeleteBuffers(6, m_VBO);
}


void ParticleSystem::ConvertMeshToIterator()
{

	glm::vec3 points[] = {
		{ -1.0f, -1.0f, -1.0f },
		{ -1.0f, -1.0f, +1.0f },
		{ -1.0f, +1.0f, +1.0f },
		{ +1.0f, +1.0f, -1.0f },
		{ -1.0f, -1.0f, -1.0f },
		{ -1.0f, +1.0f, -1.0f },

		{ +1.0f, -1.0f, +1.0f },
		{ -1.0f, -1.0f, -1.0f },
		{ +1.0f, -1.0f, -1.0f },
		{ +1.0f, +1.0f, -1.0f },
		{ +1.0f, -1.0f, -1.0f },
		{ -1.0f, -1.0f, -1.0f },

		{ -1.0f, -1.0f, -1.0f },
		{ -1.0f, +1.0f, +1.0f },
		{ -1.0f, +1.0f, -1.0f },
		{ +1.0f, -1.0f, +1.0f },
		{ -1.0f, -1.0f, +1.0f },
		{ -1.0f, -1.0f, -1.0f },

		{ -1.0f, +1.0f, +1.0f },
		{ -1.0f, -1.0f, +1.0f },
		{ +1.0f, -1.0f, +1.0f },
		{ +1.0f, +1.0f, +1.0f },
		{ +1.0f, -1.0f, -1.0f },
		{ +1.0f, +1.0f, -1.0f },

		{ +1.0f, -1.0f, -1.0f },
		{ +1.0f, +1.0f, +1.0f },
		{ +1.0f, -1.0f, +1.0f },
		{ +1.0f, +1.0f, +1.0f },
		{ +1.0f, +1.0f, -1.0f },
		{ -1.0f, +1.0f, -1.0f },

		{ +1.0f, +1.0f, +1.0f },
		{ -1.0f, +1.0f, -1.0f },
		{ -1.0f, +1.0f, +1.0f },
		{ +1.0f, +1.0f, +1.0f },
		{ -1.0f, +1.0f, +1.0f },
		{ +1.0f, -1.0f, +1.0f } };
	m_MaxParticles = 36;

	for (int i = 0; i < m_MaxParticles; i++)
	{
		int r = i / 6;
		int c = i % 6;
		//m_Particles[i].p_Target = glm::vec3(r, c, 0) * 10.0f;
		//m_Particles[i].p_Position =  glm::vec3(r, c, 0) * 10.0f;
	}

}

void ParticleSystem::Initialise(unsigned int maxPrticles, float lifespanMin,
	float lifespanMax, float velocityMin, float velocityMax, float startSize,
	float endSize, const vec4 & startColour, const vec4 & endColour)
{
	m_MaxParticles = maxPrticles;
	m_LifespanMin = lifespanMin;
	m_LifespanMax = lifespanMax;
	m_VelocityMin = velocityMin;
	m_VelocityMax = velocityMax;
	m_StartSize = startSize;
	m_EndSize = endSize;
	m_StartColour = startColour;
	m_EndColour = endColour;

	//Create particle array

	m_Positions = nullptr;
	m_Velocities = nullptr;
	m_Targets = nullptr;


	//Set Ping-Pong buffer
	m_ActiveBuffer = 0;
}

void ParticleSystem::CreateShaderBuffers()
{
	CreateBuffers();
	CreateUpdateShader();
	CreateDrawShader();
}

void ParticleSystem::Draw(float time, const mat4 & cameraTransform, const mat4 & projectionView)
{
	//Update the particles using transform feedback
	glUseProgram(m_UpdateShader);

	//Bind time information
	int location = glGetUniformLocation(m_UpdateShader, "time");
	glUniform1f(location, time);

	deltaTime = time - m_LastDrawTime;
	m_LastDrawTime = time;

	if (deltaTime > 0.01f)
		deltaTime = 0;

	location = glGetUniformLocation(m_UpdateShader, "deltaTime");
	glUniform1f(location, deltaTime);

	location = glGetUniformLocation(m_UpdateShader, "maxVelocity");
	glUniform1f(location, m_VelocityMax);

	//Bind emitters position
	location = glGetUniformLocation(m_UpdateShader, "emitterPosition");
	glUniform3fv(location, 1, &m_Position[0]);

	//Disable rasterisation
	glEnable(GL_RASTERIZER_DISCARD);

	//Bind the buffer we will update
	glBindVertexArray(m_VAO[m_ActiveBuffer]);

	//Work out the second buffer
	unsigned int secondBuffer = (m_ActiveBuffer + 1) % 2;

	//Bind buffer we will update into as points and begin transform feedback
	for (int i = 0; i < m_BuffersPerObj; ++i)
	{
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, i, m_VBO[secondBuffer * m_BuffersPerObj + i]);
	}
	glBeginTransformFeedback(GL_POINTS);
	glDrawArrays(GL_POINTS, 0, m_MaxParticles);

	//Disable transform feedback and enable rasterisation
	glEndTransformFeedback();
	glDisable(GL_RASTERIZER_DISCARD);
	for (int i = 0; i < m_BuffersPerObj; ++i)
	{
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, i, 0);
	}

	//Draw the particles using geometry shader
	glUseProgram(m_DrawShader);

	location = glGetUniformLocation(m_DrawShader, "projectionView");
	glUniformMatrix4fv(location, 1, false, &projectionView[0][0]);

	location = glGetUniformLocation(m_DrawShader, "cameraTransform");
	glUniformMatrix4fv(location, 1, false, &cameraTransform[0][0]);

	//Draw particles in second buffer
	glBindVertexArray(m_VAO[secondBuffer]);
	glDrawArrays(GL_POINTS, 0, m_MaxParticles);

	//Swap for next frame
	m_ActiveBuffer = secondBuffer;
}

unsigned int ParticleSystem::LoadShader(unsigned int type, const char * filePath)
{
	FILE* file = fopen(filePath, "rb");
	if (file == nullptr)
		return 0;

	//Read shader
	fseek(file, 0, SEEK_END);
	unsigned int length = ftell(file);
	fseek(file, 0, SEEK_SET);
	char* source = new char[length + 1];
	memset(source, 0, length + 1);
	fread(source, sizeof(char), length, file);
	fclose(file);

	unsigned int shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, 0);
	glCompileShader(shader);
	delete[] source;
	return shader;
}

void ParticleSystem::CreateBuffers()
{
	//Create Buffers
	glGenVertexArrays(2, m_VAO);
	glGenBuffers(6, m_VBO);

	//Setup first buffer
	glBindVertexArray(m_VAO[0]);

	//Positions VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(vec3), m_Positions, GL_STREAM_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Velocities VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(vec3), m_Velocities, GL_STREAM_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Targets VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[2]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(vec3), m_Targets, GL_STREAM_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);


	//Setup second buffer
	glBindVertexArray(m_VAO[1]);

	//Positions VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[3]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(vec3), m_Positions, GL_STREAM_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Velocities VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[4]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(vec3), m_Velocities, GL_STREAM_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Targets VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[5]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * sizeof(vec3), m_Targets, GL_STREAM_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);


	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ParticleSystem::CreateUpdateShader()
{
	//Create Shader
	unsigned int vertShader = LoadShader(GL_VERTEX_SHADER, "Shaders/gpuVertUpdateShader.vert");
	m_UpdateShader = glCreateProgram();
	glAttachShader(m_UpdateShader, vertShader);

	CheckShaderStatus(vertShader);

	//Specify data we will stream back
	const char* varyings[] = { "position", "velocity", "target" };
	glTransformFeedbackVaryings(m_UpdateShader, 3, varyings, GL_SEPARATE_ATTRIBS);
	glLinkProgram(m_UpdateShader);

	CheckLinkStatus(m_UpdateShader);

	//Remove un-needed handles
	glDeleteShader(vertShader);

	glUseProgram(m_UpdateShader);

	//Bind lifetime
	int location = glGetUniformLocation(m_UpdateShader, "lifeMin");
	glUniform1f(location, m_LifespanMin);

	location = glGetUniformLocation(m_UpdateShader, "lifeMax");
	glUniform1f(location, m_LifespanMax);

	location = glGetUniformLocation(m_UpdateShader, "maxVelocity");
	glUniform1f(location, m_VelocityMax);

	location = glGetUniformLocation(m_UpdateShader, "minVelocity");
	glUniform1f(location, m_VelocityMin);
}

void ParticleSystem::CreateDrawShader()
{
	unsigned int vertShader = LoadShader(GL_VERTEX_SHADER, "Shaders/gpuVertShader.vert");
	unsigned int geomShader = LoadShader(GL_GEOMETRY_SHADER, "Shaders/gpuGeometryShader.geom");
	unsigned int fragShader = LoadShader(GL_FRAGMENT_SHADER, "Shaders/gpuFragShader.frag");


	m_DrawShader = glCreateProgram();
	glAttachShader(m_DrawShader, vertShader);
	glAttachShader(m_DrawShader, fragShader);
	glAttachShader(m_DrawShader, geomShader);
	glLinkProgram(m_DrawShader);

	CheckShaderStatus(vertShader);
	CheckShaderStatus(geomShader);
	CheckShaderStatus(fragShader);

	//Remove un-needed handles
	glDeleteShader(vertShader);
	glDeleteShader(geomShader);
	glDeleteShader(fragShader);

	//Bind shader so that we can set uniforms
	glUseProgram(m_DrawShader);

	//Bind size information for interpolation
	int location = glGetUniformLocation(m_DrawShader, "sizeStart");
	glUniform1f(location, m_StartSize);

	location = glGetUniformLocation(m_DrawShader, "sizeEnd");
	glUniform1f(location, m_EndSize);

	//Bind colour information for interpolation
	location = glGetUniformLocation(m_DrawShader, "colourStart");
	glUniform4fv(location, 1, &m_StartColour[0]);

	location = glGetUniformLocation(m_DrawShader, "colourEnd");
	glUniform4fv(location, 1, &m_EndColour[0]);

}

bool ParticleSystem::CheckShaderStatus(unsigned int a_shader)
{
	// Retrieve success of compiling from shader object
	int32_t success = GL_FALSE;
	glGetShaderiv(a_shader, GL_COMPILE_STATUS, &success);

	if (success == GL_FALSE)
	{
		// Retrieve the log size
		int32_t infoLogLength = 0;
		glGetShaderiv(a_shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		// Print out the log
		glGetShaderInfoLog(a_shader, infoLogLength, 0, infoLog);
		std::printf("Failed to link shader!\n%s\n", infoLog);
		delete[] infoLog;
		return false;
	}
	return true;
}

bool ParticleSystem::CheckLinkStatus(unsigned int program)
{
	int32_t success = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &success);

	if (success == GL_FALSE)
	{
		// Retrieve the log size
		int32_t infoLogLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		// Print out the log
		glGetProgramInfoLog(program, infoLogLength, 0, infoLog);
		std::printf("Failed to link shader!\n%s\n", infoLog);
		delete[] infoLog;
		return false;
	}

	return true;
}
