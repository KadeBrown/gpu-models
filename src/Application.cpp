#include "Application.h"
#include "GameObject.h"
#include "DeltaTime.h"
#include "Debug.h"
#include "Grid.h"
#include <iostream>

Application& Application::_Instance()
{
	static Application app;
	return app;
}

bool Application::Initialise()
{
	if (glfwInit() == false)
		return false;

	int count;
	GLFWmonitor**  monitors = glfwGetMonitors(&count);

	//Create Window
	m_Window = glfwCreateWindow(1920, 1080, "Computer Graphics", monitors[0], nullptr);
	m_WindowColor = vec4(0, 0, 0, 1);

	//check if a window has been created
	if (m_Window == nullptr)
	{
		glfwTerminate();
		return false;
	}
	
	glfwMakeContextCurrent(m_Window);

	//load in the open GL functions
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		//if the GL functions did not load
		//destroy the window and quit
		glfwDestroyWindow(m_Window);
		glfwTerminate();
		return false;
	}



	//Set Background color and buffers
	glClearColor(0.25f, 0.25f, 0.25f, 1);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation(GL_FUNC_ADD);

	InitialiseMainCamera();

	m_Model = new FBX();
	m_Model->Initialise("Models/Bunny.fbx", vec3(10, 10, 10));
	m_Model_2 = new FBX();
	m_Model_2->Initialise("Models/Lucy.fbx", vec3(10, 20, 10));
	m_Model_3 = new FBX();
	m_Model_3->Initialise("Models/Dragon.fbx", vec3(10, 20, 10));
	m_Model_4 = new FBX();
	m_Model_4->Initialise("Models/kelliecharmodel.fbx", vec3(10, 20, 10));


	m_Grid = new Grid();
	m_Grid->GenerateGrid(20, 20, "Shaders/DefaultVertexShader.txt", "Shaders/DefaultFragmentShader.txt", "Textures/Water.jpg");

	m_Emitter = new ParticleSystem();
	m_Emitter->Initialise(500000, 0.1f,20.0f, 50, 100, 0.03f, 0.03f, vec4(0.3f, 0.75f, 1, 1), vec4(1, 0, 0.5f, 1));
	m_Emitter->ConvertMeshToIterator(m_Model->m_FBX);
	m_Emitter->CreateShaderBuffers();

	m_SwapTime = 10;

	return true;
}

void Application::Run()
{
	while (!glfwWindowShouldClose(m_Window) &&  m_Run)
	{
		glClearColor(m_WindowColor.r, m_WindowColor.g, m_WindowColor.b, m_WindowColor.a);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		Update();
		Render();
		m_Timer += TIME.GetDeltaTime();

		if (m_Timer > 10 && m_Timer < 11)
		{
			m_Timer = 12;
			m_Emitter->SetModelScale(1.0f);
			m_Emitter->SwitchModel(m_Model_2->m_FBX);
		}
		else if (m_Timer > 22 && m_Timer < 23)
		{
			m_Timer = 24;
			m_Emitter->SetModelScale(1.0f);
			m_Emitter->SwitchModel(m_Model_3->m_FBX);
		}
		else if (m_Timer > 34 && m_Timer < 35)
		{
			m_Timer = 36;
			m_Emitter->SetModelScale(0.1f);
			m_Emitter->SwitchModel(m_Model_4->m_FBX);
		}
		else if (m_Timer > 46 && m_Timer < 47)
		{
			m_Timer = 0;
			m_Emitter->SetModelScale(1);
			m_Emitter->SwitchModel(m_Model->m_FBX);
		}


		if (glfwGetKey(m_Window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			m_Run = false;


		glfwSwapBuffers(m_Window);
		glfwPollEvents();
	}

	Shutdown();
}

void Application::Update()
{
	TIME.UpdateDeltaTime();
	m_MainCam->Update();

	DeleteObjects();
	UpdateGameObjects();
}

void Application::Render()
{

	//m_Grid->Render();
	//m_Model->Render();
	m_Emitter->Draw(float(glfwGetTime()), m_MainCam->GetWorldTransform(), m_MainCam->GetProjectionView());
	RenderGameObjects();
}

void Application::Shutdown()
{
	m_Model->CleanupOpenGLBuffers(m_Model->m_FBX);
	m_GameObjects.clear();
	m_ObjectsToDelete.clear();
	glfwDestroyWindow(m_Window);
	glfwTerminate();
}

void Application::AddGameObject(GameObject* objToAdd)
{
	//Check to see if object has already been added to list
	objIter iterator = std::find(m_GameObjects.begin(), m_GameObjects.end(), objToAdd);

	//If not already part of the list, add it to the list
	if (iterator == m_GameObjects.end())
		m_GameObjects.push_back(objToAdd);
}

void Application::RemoveGameObject(GameObject * objToDelete)
{
	//Checks list to make sure it has not previously been added
	objIter iterator = std::find(m_ObjectsToDelete.begin(), m_ObjectsToDelete.end(), objToDelete);

	//If not already part of the list, add it to the list
	if (iterator == m_ObjectsToDelete.end())
		m_ObjectsToDelete.push_back(objToDelete);
}

void Application::UpdateGameObjects()
{
	for (objIter iterator = m_GameObjects.begin(); iterator != m_GameObjects.end(); ++iterator)
	{
		(*iterator)->Update();
	}
}

void Application::RenderGameObjects()
{
	for (objIter iterator = m_GameObjects.begin(); iterator != m_GameObjects.end(); ++iterator)
	{
		(*iterator)->Render();
	}
}

void Application::DeleteObjects()
{
	//If the list is populated
	if (m_ObjectsToDelete.size() > 0)
	{
		//Loop through list and delete each object
		for (objIter iterator = m_ObjectsToDelete.begin(); iterator != m_ObjectsToDelete.end(); ++iterator)
		{
			GameObject* obj = (*iterator);
			m_GameObjects.remove(obj);
			delete obj;
		}
		
		m_ObjectsToDelete.clear();
	}
}

void Application::InitialiseMainCamera()
{
	m_MainCam = new FlyCamera();

	m_MainCam->SetPosition(vec3(2, 0, 2));
	m_MainCam->LookAt(vec3(0, 0, 0), vec3(0, 1, 0));
	m_MainCam->SetPerspective(glm::pi<float>() * 0.25f, 16 / 9, 0.1f, 1000.f);
}

GameObject* Application::FindGameObjectWithName(std::string name)
{
	for (objIter iterator = m_GameObjects.begin(); iterator != m_GameObjects.end(); ++iterator)
	{
		if ((*iterator)->GetName() == name)
			return (*iterator);
	}
	return nullptr;
}

GameObject* Application::FindGameObjectWithTag(std::string tag)
{
	for (objIter iterator = m_GameObjects.begin(); iterator != m_GameObjects.end(); ++iterator)
	{
		if ((*iterator)->GetTag() == tag)
			return (*iterator);
	}
	return nullptr;

}

std::list<GameObject*> Application::FindAllGameObjectsWithName(std::string name)
{
	std::list<GameObject*> foundObjects;
	for (objIter iterator = m_GameObjects.begin(); iterator != m_GameObjects.end(); ++iterator)
	{
		if ((*iterator)->GetName() == name)
			 foundObjects.push_back((*iterator));
	}
	return foundObjects;
}

std::list<GameObject*> Application::FindAllGameObjectsWithTag(std::string tag)
{
	std::list<GameObject*> foundObjects;
	for (objIter iterator = m_GameObjects.begin(); iterator != m_GameObjects.end(); ++iterator)
	{
		if ((*iterator)->GetTag() == tag)
			foundObjects.push_back((*iterator));
	}
	return foundObjects;
}

