#include "FBX.h"
#include <gl_core_4_4.h>
#include "Application.h"
#include "FlyCamera.h"
#include "stb-master\stb_image.h"
#include "glm\ext.hpp"


void FBX::Initialise(const char * filePath, vec3 position, const char * customTexture)
{
	m_DefaultTexture.LoadTexture(customTexture);
	m_Position = position;

	m_GlobalTransform = glm::scale(m_GlobalTransform, vec3(0.01f));
	m_GlobalTransform = glm::translate(m_GlobalTransform, m_Position);


	//set custom texture
	m_CustomTexturePath = customTexture;
	m_Program = new Program();
	m_Program->CreateNewProgram("Shaders/vShaderSource.txt", "Shaders/fShaderSource.txt");
	m_FBX = new FBXFile();
	m_FBX->load(filePath);
	m_FBX->initialiseOpenGLTextures();
	CreateOpenGLBuffers(m_FBX);
	CreateDefaultTexture();
}

void FBX::CreateOpenGLBuffers(FBXFile* fbx)
{
	//Create the GL VAO/VBO/IBO data for each mesh
	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);

		//Storage for the opengl data in 3 unsigned int
		unsigned int* glData = new unsigned int[3];

		glGenVertexArrays(1, &glData[0]);
		glBindVertexArray(glData[0]);
		glGenBuffers(1, &glData[1]);
		glGenBuffers(1, &glData[2]);
		glBindBuffer(GL_ARRAY_BUFFER, glData[1]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glData[2]);
		glBufferData(GL_ARRAY_BUFFER, mesh->m_vertices.size() * sizeof(FBXVertex), mesh->m_vertices.data(), GL_STATIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->m_indices.size() * sizeof(unsigned int), mesh->m_indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //Position
		glEnableVertexAttribArray(1); //TexCoord
		glEnableVertexAttribArray(3); //Normal

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::PositionOffset);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::TexCoord1Offset);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char*)0) + FBXVertex::NormalOffset);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		mesh->m_userData = glData;
	}
}

void FBX::CleanupOpenGLBuffers(FBXFile* fbx)
{
	//Clean up the vertex data attached to each mesh
	for (unsigned int i = 0; i < fbx->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = fbx->getMeshByIndex(i);
		unsigned int* glData = (unsigned int*)mesh->m_userData;
		glDeleteVertexArrays(1, &glData[0]);
		glDeleteBuffers(1, &glData[1]);
		glDeleteBuffers(1, &glData[2]);
		delete[] glData;
	}
}

void FBX::CreateDefaultTexture()
{
	//If there is no texture, dont attempt to load the texture
	if (m_CustomTexturePath != nullptr)
	{
		m_DefaultTexture.LoadTexture(m_CustomTexturePath);
	}
}

void FBX::Render()
{


	glUseProgram(m_Program->GetProgramID());

	//Bind the camera
	int location = glGetUniformLocation(m_Program->GetProgramID(), "ProjectionView");
	glUniformMatrix4fv(location, 1, GL_FALSE, &(APPLICATION.GetFlyCamera()->GetProjectionView()[0][0]));

	location = glGetUniformLocation(m_Program->GetProgramID(), "LightDir");
	glUniform3fv(location, 1, glm::value_ptr(vec3(1, 0, 1)));

	location = glGetUniformLocation(m_Program->GetProgramID(), "LightColour");
	glUniform3fv(location, 1, glm::value_ptr(vec3(1.0f, 1.0f, 1.0f)));

	location = glGetUniformLocation(m_Program->GetProgramID(), "CameraPos");
	glUniform3fv(location, 1, glm::value_ptr(APPLICATION.GetFlyCamera()->m_Position));

	location = glGetUniformLocation(m_Program->GetProgramID(), "SpecPow");
	glUniform1f(location, 3);


	//Bind our vertex array object and draw the mesh
	for (unsigned int i = 0; i < m_FBX->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = m_FBX->getMeshByIndex(i);

		//Create a texture ID
		GLuint texId = m_DefaultTexture.GetTexture();

		FBXTexture* pDiffuse = mesh->m_material->textures[FBXMaterial::DiffuseTexture];

		//If there is a texture assigned to the model
		if (pDiffuse != nullptr)
		{
			//Change the texID
			texId = pDiffuse->handle;
		}

		//Load the texture according to the texture ID
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texId);

		location = glGetUniformLocation(m_Program->GetProgramID(), "diffuse");
		glUniform1i(location, 0);

		location = glGetUniformLocation(m_Program->GetProgramID(), "ModelTransform");
		glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(m_GlobalTransform));

		unsigned int* glData = (unsigned int*)mesh->m_userData;
		glBindVertexArray(glData[0]);
		glDrawElements(GL_TRIANGLES,
			(unsigned int)mesh->m_indices.size(), GL_UNSIGNED_INT, 0);

	}

	//Unbind texture location
	glBindTexture(GL_TEXTURE_2D, 0);
}

void FBX::SetPosition(glm::vec3 position)
{
	m_GlobalTransform = glm::translate(m_GlobalTransform, position);
}
