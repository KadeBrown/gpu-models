#version 410

//Input points, output quad
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

//Input data from vertex shader
in vec3 position[];
in vec3 target[];

//Output to fragment shader
out vec4 Colour;

uniform mat4 projectionView;
uniform mat4 cameraTransform;

uniform float sizeStart;
uniform float sizeEnd;

uniform vec4 colourStart;
uniform vec4 colourEnd;

void main()
{
	//Interpolate Colour
	vec4 tempColour = mix(colourStart, colourEnd, distance(target[0], position[0]) / 5);
	Colour = tempColour; //vec4(tempColour.x + 0.5, tempColour.y, tempColour.z + 0.9, 1);
	//Calculate Size
	float halfSize = sizeEnd * 0.5;
	
	//Create Corners of Quad
	vec3 corners[4];
	corners[0] = vec3(halfSize,  -halfSize, 0);
	corners[1] = vec3(halfSize,   halfSize, 0);
	corners[2] = vec3(-halfSize, -halfSize, 0);
	corners[3] = vec3(-halfSize,  halfSize, 0);
	
	//Billboard
	vec3 zAxis = normalize( cameraTransform[3].xyz - position[0]);
	vec3 xAxis = cross( cameraTransform[1].xyz, zAxis);
	vec3 yAxis = cross( zAxis, xAxis);
	
	mat3 billboard = mat3(xAxis, yAxis, zAxis);
	
	//Emit the 4 vertices for the quad
	gl_Position = projectionView * vec4(billboard * corners[0] + position[0], 1);
	EmitVertex();
	
	gl_Position = projectionView * vec4(billboard * corners[1] + position[0], 1);
	EmitVertex();
	
	gl_Position = projectionView * vec4(billboard * corners[2] + position[0], 1);
	EmitVertex();
	
	gl_Position = projectionView * vec4(billboard * corners[3] + position[0], 1);
	EmitVertex();
}