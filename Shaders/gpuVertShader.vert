#version 410

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Velocity;
layout(location=2) in vec3 Target;

out vec3  position;
out vec3 velocity;
out vec3 target;


void main()
{
	position = Position;
	velocity = Velocity;
	target = Target;

}