#version 410

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Velocity;
layout(location=2) in vec3 Target;

out vec3 position;
out vec3 velocity;
out vec3 target;

uniform float deltaTime;

//uniform float time;
uniform float maxVelocity;
uniform float minVelocity;
//uniform vec3 emitterPosition;

void main()
{
	float dist = distance(Target, Position) * 100;
	dist = clamp(dist, minVelocity, maxVelocity);

	velocity = normalize(Target - Position) * deltaTime * dist * 50;
	position = Position + velocity * deltaTime;
	if(distance(Target, Position) < 0.1)
	{
		position = Target;
		velocity = vec3(0,0,0);
	}

	target = Target;


}